/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useRef,useState,useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  Button,
  FlatList,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
  Image
} from 'react-native';
import { WebView } from 'react-native-webview'
import Icon from 'react-native-vector-icons/Ionicons'
import {Container,Header,Left,Right} from 'native-base'

import {
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import 'react-native-gesture-handler';
import { DrawerActions, NavigationContainer, useLinkProps } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useIsFocused } from "@react-navigation/native";
import AnimatedLoader from "react-native-animated-loader";

const Tab = createBottomTabNavigator();
const STORAGE_KEY = '@name'

function HomeScreen() {
  return (
      <Tab.Navigator screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === "Weather") {
            iconName = focused ? 'cloud' : 'cloud-outline';
          } else if (route.name === "Google") {
            iconName = focused ? 'search-circle' : 'search-circle-outline';
          }
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: 'black',
        inactiveTintColor: 'gray',
      }}>

        <Tab.Screen name="Weather" component={WeatherScreen} />
        <Tab.Screen name="Google" component={SearchScreen} />
      </Tab.Navigator>
    
  );
}

const Stack = createStackNavigator();
const DATA = [
  {
    id: '1',
    title: 'About',
  },
  {
    id: '2',
    title: 'Profile',
  },
];

const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);

function FlatListScreen ({navigation}) {
  const renderItem = ({ item }) => (
    <TouchableOpacity onPress={() => navigation.navigate(item.title)}>
      <Item title={item.title} />
      </TouchableOpacity>
  );

  return (
    <FlatList
    style = {styles.flatlist}
    data={DATA}
    renderItem={renderItem}
    keyExtractor={item => item.id}
  />
);
}


function SettingsScreen({ navigation }) {
return (
  <Stack.Navigator>
    <Stack.Screen name="Settings" component={FlatListScreen} options={{headerTitle:"Settings", headerLeft: () => (
			<Icon name="md-menu" size={30} style={{marginLeft:10,marginTop:5}}onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())} />
	) }}/>
    <Stack.Screen name="About" component={AboutScreen} options={{title:"About"}} />
    <Stack.Screen name="Profile" component={ProfileScreen} 
    options={{ headerRight: () => (
      <TouchableOpacity   onPress={() => navigation.navigate('Edit')} >
        <Text style={styles.edit} >Edit</Text>
      </TouchableOpacity>)
    }}
    />  
    <Stack.Screen name="Edit" component={EditScreen} />
  </Stack.Navigator>
)
}


function AboutScreen() {
  return(
    <View style={{ flex: 1, textAlign:'justify'}}>
      <Text style={{textAlign:'justify',marginHorizontal:10,marginVertical:10,fontSize:18}}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse posuere dignissim nisi sed varius. Mauris sed odio nisi.
        Donec ut mattis felis. Proin non nulla tristique, bibendum lectus eu, cursus sapien. 
        Integer ultrices porttitor venenatis. Ut mauris dui, suscipit eu maximus a, euismod eget neque. 
        {"\n \n \n"}
        Sed luctus dolor nec convallis interdum. Ut tincidunt neque in leo luctus, sit amet viverra erat fringilla. 
        Nunc placerat at dolor id dapibus. Vestibulum nec tellus sed urna rhoncus ultricies. Etiam lacus turpis, 
        efficitur ac imperdiet molestie, fringilla id mi. Nunc lobortis imperdiet nulla, quis egestas risus feugiat eget.
        {"\n \n \n"}
        Aenean finibus neque auctor mauris dapibus sodales. Aenean dictum ligula ipsum. Morbi eu augue ornare, consectetur ipsum feugiat, 
        consectetur mauris. Quisque ut arcu suscipit, blandit nulla et, dapibus quam. Ut mollis nulla tortor, sit amet dictum enim fringilla
        sit amet. Maecenas gravida nisi non eros lacinia tempor. Aliquam vel hendrerit nisi, eu egestas diam.
      </Text>
    </View>
  )

}

function ProfileScreen({navigation}) {
  const [name,setName] = useState(null)
	const isFocused = useIsFocused();

    const readData = async () => {
    try {
        const userName = await AsyncStorage.getItem(STORAGE_KEY)
        console.log(userName)
        if (userName !== null) {
          setName(userName)
        }
        else {
          setName("<Not Set>")
        }
      } catch (e) {
        alert('Could not fetch name!')
      }
    }
    useEffect(() => {
      readData()
	}, [isFocused])

    return (
      <View style={{ flex: 1, margin:20 }}>
        <Text style={{fontSize:18}}> <Text style={{fontWeight:'bold'}}>Name</Text>: {name} </Text> 
      </View>
    )

}

  const storeData = async (name,navigation) => {
    try {
      const jsonValue = name
      await AsyncStorage.setItem(STORAGE_KEY, jsonValue)
      navigation.navigate("Profile")
    } catch (e) {
      alert("Could not store the value!")
    }
  }

function EditScreen({navigation}) {
  const [name,setName] = useState(null);
  const isFocused = useIsFocused();

  const readData = async () => {
  try {
	  const userName = await AsyncStorage.getItem(STORAGE_KEY)
	  //console.log(userName)
	  if (userName !== null) {
		setName(userName)
	  }
	} catch (e) {
	  alert('Could not fetch name!')
	}
  }
  useEffect(() => {
	readData()
  }, [isFocused])
  return (
    <View style={{ flex: 1, margin:20}}>
      <Text style={{fontWeight:'bold'}}>Enter a name</Text>
      <TextInput style={{borderColor:'black',borderWidth:1,marginVertical:10, fontSize:18}} onChangeText={name => setName(name)} defaultValue={name}></TextInput>
      <TouchableOpacity onPress={() => {storeData(name,navigation)}} style={styles.saveButton}>
        <Text style={{color:'white',fontSize:18,marginTop:5}}>Save</Text>
      </TouchableOpacity>
    </View>
  )
}

function WeatherScreen({ navigation }) {
  const [weather,setWeather] = useState(null);
  const [loader,setLoader] = useState(false);
  const [location,setLocation] = useState(null);
  const api_key = '2ca0aad18afe1d85d679b037a7ca2be8'

  const fetchWeather = async() => {
    console.log(location)
    fetch('https://api.openweathermap.org/data/2.5/weather?q='+location+'&appid='+api_key)
    .then(response => response.json())
    .then(response => {
      setWeather(response["weather"][0])
      setLoader(false)
    }).catch( err => {
      setWeather("Sorry, could not fetch weather for the given location!")
      setLoader(false)
    })
  }

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <TextInput style={{borderBottomWidth:0.7,fontSize:16,padding:0,marginBottom:15}} onChangeText={location => setLocation(location)} placeholder="Enter a location" />
      <Button title="Get weather" onPress={() => {fetchWeather(); setLoader(true)}}/> 
      { loader ? <ActivityIndicator size="large" color="black" /> : 
        <Text>{"\n"} 
        { weather ? weather.description ? 
        <View>
        <Image style={{height:100,width:100,alignSelf:'center',backgroundColor:'#eee'}} source={{uri: 'http://openweathermap.org/img/w/' + weather.icon +'.png'}} />
        <Text style={{fontSize:16,alignSelf:'center',textTransform:'capitalize'}}> {weather.description} </Text>
      </View>
      : weather :''}
        </Text> 
      }
     
  </View>
  )
}


function SearchScreen({ navigation }) {
  const webview = useRef(null)

  return (
    <WebView
      ref={webview}
      startInLoadingState
      style={{ flex: 1 }}
      source={{ uri: "https://google.com" }}
    />
  )
}

function StackHomeScreen({navigation}) {
	return (
		 <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} options={{headerTitle:"Home", headerLeft: () => (
			<Icon name="md-menu" size={30} style={{marginLeft:10,marginTop:5}}onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}/> ) }} />
      </Stack.Navigator> 
	)
}
const Drawer = createDrawerNavigator();


const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
          {global.HermesInternal == null ? null : (
            <View style={styles.engine}>
              <Text style={styles.footer}>Engine: Hermes</Text>
            </View>
          )}
 
      <NavigationContainer>
		    <Drawer.Navigator initialRouteName="StackHome">
          <Drawer.Screen name="StackHome" component={StackHomeScreen} options={{title:"Home"}} />
          <Drawer.Screen name="Settings" component={SettingsScreen} />
        </Drawer.Navigator>
    	</NavigationContainer>

    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  edit: {
    fontSize:18,
    paddingHorizontal:10,
    paddingVertical:5,
    color:'blue',
    marginRight:20,
    textDecorationLine:'underline',
    textDecorationColor:'blue',
  },
  saveButton: {
    height:40,
    width:'33%',
    marginLeft:'33%',
    backgroundColor:'#2196F3',
    alignItems:'center',
  },
  item: {
    backgroundColor: '#38B865',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
    color:'black',
    textAlign:'center'
  },
  flatlist: {
    marginTop:10,
  }
});

export default App;
